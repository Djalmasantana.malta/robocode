package MR;

import robocode.*;

public class MainRobot extends AdvancedRobot {
	double velocity = 100;
	double looping = 20;
	boolean go = true;
	boolean hitWall = true;
	int gunIncrement = 7;
	public void run() {
		turnGunLeft(90);
		while (true) {
			for (int i = 0; i < looping; i++) {
				turnGunRight(gunIncrement);
			}
			gunIncrement *= -1;
			if (go){
				ahead(velocity);
			}
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		stop();
		fireAdjust(e.getDistance());
		scan();
		resume();
	}

	public void fireAdjust(double distance) {
		if (distance >= 70){
			fire(2);
			ahead(velocity - 50);
		} else {
			fire(3);
			ahead(velocity);
		}
	}

	public void onHitByBullet(HitByBulletEvent e) {
		if (!hitWall) {
			go = false;
			turnRight(90);
			ahead(velocity);
			go = true;
		}
	}

	public void onHitWall(HitWallEvent e) {
		hitWall = true;
		turnRight(90);
		ahead(velocity);
		hitWall = false;
	}	
}
