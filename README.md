**MainRobot (Robô Principal)**

 - O MainRobot se movimenta de acordo com o que acontece no campo de batalha. se houver uma colisão ele é capaz de mudar sua rota, em caso de ser atingido por disparos, como contra medida, tem sua rota alterada para diminuir as chances de ser atingido novamente, o seu disparo é efetuado de acordo a distância em que seu adversário se encontra assim poupando energia.

- Inicialmente é instanciado 5 variáveis sendo 2 do tipo double(velocity e looping), 2 do tipo boolean (go e hitWall) e 1 do tipo int (gunIncrement):
    - **velocity:** Responsável por definir a velocidade do robô;
    - **looping:** Responsável pelo tamanho do loop “for” que faz o armamento girar para direita e esquerda;
    - **go:** Responsável por indicar quando o robô pode voltar a andar para frente, logo depois de fazer as manobras para desvio de trajetória;
    - **hitWall:** Responsável por indicar que o robô atingiu  uma parede e está efetuando correções em sua trajetória, usado para bloquear outras ações até que termine;
    - **gunIncrement:** Responsável por setar o incremento usado no loop “for” do movimento do armamento;

-  Através da função “onScannedRobot” o robô pode identificar um adversário e efetuar os ajustes de disparo através da função fireAdjust (ajuste de fogo) na qual vai decidir o poder do disparo de acordo com a distância do alvo adquirida através da função ”getDistance()”, e, após o disparo, efetua uma manobra em linha reta para mudar sua posição com o uso da função “ahead()”;

- Com a função “onHitByBullet” o MainRobot  é capaz de efetuar pequenas correções de trajetória no intuito de amenizar as chances de ser atingido novamente com o uso de funções de movimentação “ahead()” e “turnRight()”;

- Com a função onHitWall o robô é capaz de efetuar manobras com o uso de funções de movimentação “ahead()” e “turnRight()” após colidir com paredes escapando de ficar parado e se tornar um alvo fácil;

**Pontos Positivos**

- O MainRobot não fica parado no meio da arena, pode se locomover e mudar de trajetória de acordo com o que acontece;

- Pode efetuar disparos poderosos mas ao mesmo tempo poupar energia, controlando o poder do disparo de acordo a distância do alvo;

- Pode se movimentar sem medo de ficar preso por paredes, é capaz de realizar manobras para desviar delas;

**Pontos Negativos**

- Não consegue atingir com precisão alvos em movimento;

- Não consegue interceptar a trajetória do alvo;

- Sua capacidade de identificar alvos assim como guardar a posição do mesmo ao fazer manobras é bastante reduzida;

**Opinião sobre a fase**

- Com a atividade desenvolvida nessa fase foi possível aprimorar o conhecimento sobre a linguagem java, e também ter um maior entendimento sobre fluxo de execução e lógica, com o desenvolvedor tendo que usar destes meios para permitir que o seu robô batalhe com outros de forma autônoma.






